# Android AI应用开发：人脸检测

## A. 项目描述
本项目使用预训练的Google ML模型进行人脸检测。这个项目展示了单个人脸检测和多个面孔识别的示例。即使有些人侧脸对相机，我们仅能在图像中看到其侧面，模型也能成功检测到。

> Google的`ML Kit`是一个用于移动应用开发的机器学习工具包，旨在帮助开发者轻松集成机器学习功能到他们的Android和iOS应用中。
> `ML Kit`提供了一系列预训练的机器学习模型和API，使开发者可以在不需要深入了解机器学习复杂性的情况下，实现一些先进的功能。
> `ML Kit`的目标是降低开发者集成机器学习功能的门槛，使更多的移动应用能够受益于这些先进技术，提供更丰富和智能的用户体验。

## B. 开发工具
- Android Studio Koala 
- Kotlin
- Gradle 8.7

## C. 代码设计
### 界面设计

![UI效果](img_src/FaceDection/fd1.png)

界面上部分用于展示人物图片，底部为“人脸检测”按钮。点击底部按钮，执行检测任务，执行完毕在图片中显示结果（绘制一个矩形区域将面部标记出来）。

### 调用人脸检测API
人脸检测API 可以通过`FaceDetectorOptions`对象来访问。
 ```kotlin
val options = FaceDetectorOptions.Builder() 
	.setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_FAST)
            .build()

val detector = FaceDetection.getClient(options)
```
使用`FaceDetectorOptions.Builder()`创建**人脸检测器**的参数选项，可以在此处设置各种工作模式，然后创建**人脸检测器**对象。

接下来，创建图像输入对象，并将`Bitmap`传递给它。
可以通过调用`detector.process`并将输入图像传递给检测器来获取检测结果。
如果检测成功，将会收到一个`onSuccessListener`的回调函数，其中包含一个人脸列表；如果失败，你会得到一个`onFailureListener`用于跟踪异常。

```kotlin
        val image = InputImage.fromBitmap(bitmap, 0)
        val result = detector.process(image)
            .addOnSuccessListener { faces ->
                // 任务执行成功
                handleFaceDetectResult(faces, img, bitmap)
            }
            .addOnFailureListener { e ->
                // 任务执行失败
                Log.e("MainActivity", e.toString())
            }
```

## D. 项目演示

![动画效果](img_src/FaceDection/fd.gif)
## E. 项目源码

关注公众号『数字森林』，后台发送关键字：<mark>人脸检测</mark>，获取项目源码。

