# Android AI应用开发：图像标记

## A. 项目描述
图像分类在机器学习界是一个众所周知的概念，从最简单的意义上讲，当你向计算机显示图像，它告诉你该图像包含什么时，就会发生图像分类。`ML Kit` 中的图像标记更进一步，以概率级别为你提供在图像中看到的事物的列表。

## B. 开发工具
- Android Studio Koala 
- Kotlin
- Gradle 8.7

## C. 代码设计
### 界面设计

![界面设计](img_src/ImageLabelling/ilabel2.png)

界面上方是一个`ImageView`，用于呈现示例图片；用户点击中间按钮，调用图像标记处理代码；待处理完毕，将结果展示在下方的 `TextView` 中。

### 图像标记处理代码
当用户单击按钮时，此代码将从ML Kit创建一个图像标记器：
```kotlin
val labeler = ImageLabeling.getClient(ImageLabelerOptions.DEFAULT_OPTIONS)
```
然后从`Bitmap`（用于显示图像）创建一个输入图像对象：
```kotlin
val image = InputImage.fromBitmap(bitmap!!, 0)
```

`labeler.process` 调用标记器来处理图像，并向其中添加两个监听器。
如果处理成功，则将触发成功监听器，否则将触发失败监听器。
当图像标记器成功时，它将返回一个标签列表。这些标签有一个带有描述标签的文本的文本属性，以及一个值为0到1的置信度属性，其中包含标记项存在的概率。

```kotlin
labeler.process(image)
                .addOnSuccessListener { labels ->
                    // 任务执行成功
                    for (label in labels) {
                        val text = label.text
                        val confidence = label.confidence
                        outputText += "$text : $confidence\n"
                        //val index = label.index
                    }
                    txtOutput.text = outputText
                }
                .addOnFailureListener { e ->
                    // 任务执行失败
                    Log.e("MainActivity", e.toString())
                }
```

## D. 项目演示

![动画效果](img_src/ImageLabelling/ImageLabelling.gif)

## E. 项目源码

关注公众号『数字森林』，后台发送关键字：<mark>图像标记</mark>，获取项目源码。

