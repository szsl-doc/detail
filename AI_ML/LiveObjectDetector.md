# Android AI应用开发：移动检测

> 基于Google ML模型的Android移动物体检测应用——检测、跟踪视频中的物体

## A. 项目描述
ML Kit物体检测器可以对视频流进行操作，能够检测视频中的物体并在连续视频帧中跟踪该物体。

![动画效果](img_src/LiveObjectDetector/charge.gif)

相机捕捉视频时，检测到移动物体并为其生成一个边界框，并分配一个跟踪ID。即使在物体在视野中移动时，后续帧中的边界框会根据其新位置更新，但跟踪ID保持不变。这意味着尽管物体在不同帧内的位置和相机角度有所不同，但它仍被识别为同一物体。

## B. 开发工具
- Android Studio Koala 
- Kotlin
- Gradle 8.7

## C. 代码设计
```xml
<FrameLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:padding="8dp">

    <androidx.camera.view.PreviewView
        android:id="@+id/viewFinder"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:layout_gravity="center" />

    <com.szsl.detector.GraphicOverlay
        android:id="@+id/graphicOverlay"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_gravity="center" />
</FrameLayout>
```
在FrameLayout中，首先是一个`PreviewView`控件，用于呈现相机的视频流。其上层是一个自定义控件`GraphicOverlay`，它在预览视图的上层提供了一个叠加层，允许在其上绘制边界框图形。

自定义的`GraphicOverlay`类的工作是管理一组图形对象，这些对象由边界框及其标签组成，并在画布上进行绘制。

首先，设置预览视图（`viewFinder`）以渲染相机的帧流：
```kotlin
            // 预览
            val preview = Preview.Builder()
                .build()
                .also {
                    it.setSurfaceProvider(viewFinder.surfaceProvider)
                }
```

接下来是图像分析器。当你调用`setAnalyzer`时，CameraX会逐帧调用该函数，对图像进行处理。需要指定一个处理分析的类；在这里，使用了一个名为`ObjectAnalyzer`的类，它将利用物体检测API对帧进行处理：
```kotlin
            val imageAnalyzer = ImageAnalysis.Builder()
                .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                .build()
                .also {
                    it.setAnalyzer(cameraExecutor, ObjectAnalyzer(graphicOverlay))
                }
```

一旦这些准备就绪，将它们绑定到相机的生命周期上。这样CameraX就能够管理它们，分别用于渲染预览和逐帧处理：
```kotlin
                // 相机绑定
                cameraProvider.bindToLifecycle(
                    this, cameraSelector, preview, imageAnalyzer
                )
```

### 小结
使用CameraX，可以指定一个预览视图和一个分析器。在启用流模式时，分析器调用ML Kit物体检测器。检测到的物体被用来创建表示边界框的对象，并将这些对象添加到叠加层中。
这里使用了ML Kit的通用模型，因此分类方式相对较少——它只检测物体并为每个物体分配一个ID。


## D. 项目演示

![动画效果](img_src/LiveObjectDetector/charge.gif)

![效果1](img_src/LiveObjectDetector/cha_00001.jpg)

![效果2](img_src/LiveObjectDetector/cha_00002.jpg)

## E. 项目源码

关注公众号『数字森林』，后台发送关键字：<mark>移动检测</mark>，获取项目源码。

