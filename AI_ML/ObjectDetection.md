# Android AI应用开发：物体检测

## A. 项目描述
`ML Kit`的默认模型在物体检测方面表现非常出色。当你将图像传递给物体检测器时，它会返回一个物体列表，其中包含边界框，这些边界框可用于确定图像中物体可能的位置。

本项目首先对图像中的物体进行分类检测，获取分类物体的位置区域，然后结合图像标记，逐个获取单个物体的标签。

## B. 开发工具
- Android Studio Koala 
- Kotlin
- Gradle 8.7

## C. 代码设计
### 界面设计

![UI设计](img_src/ObjectDetection/objDet2.png)

界面上方是一个`ImageView`，用于呈现示例图片；用户点击中间按钮，调用物体检测处理代码；待处理完毕，将结果展示在下方的 `TextView` 中。

### 物体检测处理
ML Kit物体检测器提供多种物体检测方法，这些方法由`ObjectDetectorOptions`对象控制。该检测器是一个强大的API，不仅可以检测物体，还能在视频流中跟踪它们，实现逐帧跟踪功能。

```kotlin
val options =
            ObjectDetectorOptions.Builder()
                .setDetectorMode(ObjectDetectorOptions.SINGLE_IMAGE_MODE)
                .enableMultipleObjects()
                .build()
```

创建物体检测器，将`Bitmap`转换为InputImage，并使用物体检测器进行处理。
这将在成功时返回检测到的物体列表，或在失败时返回异常物体。
```kotlin
val objectDetector = ObjectDetection.getClient(options)
            var image = InputImage.fromBitmap(bitmap!!, 0)
            txtOutput.text = ""
            objectDetector.process(image)
                    .addOnSuccessListener { detectedObjects ->
                        // 任务执行成功
                        getLabels(bitmap, detectedObjects, txtOutput)
                        bitmap?.apply{
                            img.setImageBitmap(drawWithRectangle(detectedObjects))
                        }

                    }
                    .addOnFailureListener { e ->
                        // 任务执行失败
                        Log.e("MainActivity", e.toString())
                    }
```

用户点击按钮后，调用物体检测器以获取图像中物体的边界框。然后，使用这些边界框裁剪图像，得到定义的子图像，并将其传递给图像标记器。

#### 标记物体
使用已有的边界框创建新的临时图像（`croppedBitmap`），将其传递给图像标记器，并返回结果。重复此操作以处理每个边界框（即每个物体），以获取每个检测到物体的详细标签！
```kotlin
val labeler =
        ImageLabeling.getClient(ImageLabelerOptions.DEFAULT_OPTIONS)
    for(obj in objects) {
        val bounds = obj.boundingBox
        val croppedBitmap = Bitmap.createBitmap(
            bitmap,
            bounds.left,
            bounds.top,
            bounds.width(),
            bounds.height()
        )
        var image = InputImage.fromBitmap(croppedBitmap!!, 0)
        labeler.process(image)
            .addOnSuccessListener { labels ->
                // 任务执行成功
                var labelText = ""
                if(labels.isNotEmpty()) {
                    labelText = txtOutput.text.toString()
                    for (thisLabel in labels){
                        labelText += thisLabel.text + " , "
                    }
                    labelText += "\n"
                } else {
                    labelText = "Not found." + "\n"
                }
                txtOutput.text = labelText.toString()
            }
    }
```

该代码遍历每个检测到的物体，使用边界框创建名为`croppedBitmap`的新位图。接下来，它使用一个设置了默认选项的图像标记器（称为`labeler`）处理新图像。处理成功后，从标签中获取多个结果，并将这些标签写入逗号分隔的字符串，最终呈现在`TextView`中。

## D. 项目演示

![动画效果](img_src/ObjectDetection/objDet.gif)

## E. 项目源码

关注公众号『数字森林』，后台发送关键字：<mark>物体检测</mark>，获取项目源码。

