# 源码项目详情

## Android 项目实战，课程设计，大作业，源码

1. [切鱼达人，Android休闲游戏开发 🔗](https://gitlab.com/szsl-doc/detail/-/blob/main/fish-cutting-master.md)
![游戏动效](https://gitlab.com/szsl-doc/detail/-/raw/main/images/fishcut/fish4.gif)

----
2. [打砖块，Android休闲小游戏开发 🔗](https://gitlab.com/szsl-doc/detail/-/blob/main/hitBricks.md)
![动效](https://gitlab.com/szsl-doc/detail/-/raw/main/images/hitbricks/g-hitbricks.gif)

---
3. [“牛弹琴”，Android 弹钢琴 app 开发 🔗](https://gitlab.com/szsl-doc/detail/-/blob/main/piano.md)
![动效](https://gitlab.com/szsl-doc/detail/-/raw/main/images/piano/piano.gif)

---
4. [2048 数字合成大作战，Android小游戏开发 🔗](https://gitlab.com/szsl-doc/detail/-/blob/main/2048.md)
![动效](https://gitlab.com/szsl-doc/detail/-/raw/main/images/2048/sg2048.gif)

---
5. [开心打地鼠，Android小游戏开发 🔗](https://gitlab.com/szsl-doc/detail/-/blob/main/tap-gopher.md)
![动效](https://gitlab.com/szsl-doc/detail/-/raw/main/images/tap-gopher/tap-gopher.gif)

---
6. [寻路迷宫，Android休闲益智小游戏开发 🔗](https://gitlab.com/szsl-doc/detail/-/blob/main/maze.md)
![动效](https://gitlab.com/szsl-doc/detail/-/raw/main/images/maze/minos.gif)

---
7. [水果连连看，Android休闲小游戏开发 🔗](https://gitlab.com/szsl-doc/detail/-/blob/main/fruits-llk.md)
![游戏动效](https://gitlab.com/szsl-doc/detail/-/raw/main/images/fruits-llk/fruts.gif)

---
8. [单词精灵，Android 记单词 app 开发 🔗](https://gitlab.com/szsl-doc/detail/-/blob/main/wordgenie.md)
![动效](https://gitlab.com/szsl-doc/detail/-/raw/main/images/wordgenie/wordgenie.gif)

---
9. [台球王子，Android小游戏开发 🔗](https://gitlab.com/szsl-doc/detail/-/blob/main/billiards_prince.md)
![游戏动效](https://gitlab.com/szsl-doc/detail/-/raw/main/images/billiards_prince/billiards_prince_1.gif)

---
---
> [Android Studio 下载地址](https://gitlab.com/szsl-doc/detail/-/blob/main/get/android_studio.md)

