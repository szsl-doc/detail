# 台球王子，Android小游戏开发
> 使用 Android Studio 开发了一款休闲游戏 —— 《台球王子》

![游戏动效](https://gitlab.com/szsl-doc/detail/-/raw/main/images/billiards_prince/billiards_prince_1.gif)

#### 关键词：<mark>台球</mark>

## A. 项目描述
台球作为一项优雅、策略性强的运动，在众多游戏类型中却相对较少。因此，开发《台球王子》小游戏，可以让更多玩家能够轻松享受到台球的乐趣。游戏采用高清的游戏画面表现，结合流畅的操作手感，让玩家仿佛置身于真实的台球场地中，享受沉浸式的游戏体验。

## B. 开发工具
- Android Studio Dolphin | 2021.3.1 Patch 1
- Java , JDK 11.0.13
- Gradle , gradle-7.4

## C. 代码设计
`GameView`是游戏界面自定义示图，继承自`SurfaceView`，它包含了球杆、球桌、台球，以及力度控制条、角度调控按钮、击球按钮、计时器等元素，实现了台球游戏的各个要素，模拟台球游戏的功能；

在`surfaceCreated` 函数中，示图被创建时，初始化各种资源：创建画笔、创建所有线程、初始化位图资源、初始化声音资源，创建球台对象列表、球杆对象、球台对象、力度控制条对象等等。

在`drawUI`函数中，绘制游戏界面的各个元素，该函数被`onDraw`函数以及绘制线程所调用：
```java
    protected void drawUI(Canvas canvas) {
        canvas.drawColor(Color.BLACK);//整个屏幕背景色
        canvas.drawBitmap(bgBmp, 0, 0, paint);//游戏界面背景
        table.drawSelf(canvas, paint);//绘制球台
        //绘制所有球
        List<Ball> alBallsTemp = new ArrayList<Ball>(alBalls);
        for (Ball b : alBallsTemp) {
            b.drawSelf(canvas, paint);
        }
        cue.drawSelf(canvas, paint);//绘制球杆
        strengthBar.drawSelf(canvas, paint);//绘制力度条
        goBtn.drawSelf(canvas, paint);//绘制GO按钮
        leftBtn.drawSelf(canvas, paint);//绘制左按钮
        rightBtn.drawSelf(canvas, paint);//绘制右按钮
        aimBtn.drawSelf(canvas, paint);//绘制目标按钮
        if (activity.coundDownModeFlag) {
            timer.drawSelf(canvas, paint);//绘制时间
        }
    }
```

`onTouchEvent`函数，处理用户在屏幕上的的触摸事件：
滑动事件：响应用户对力度控制条的调整；用户调整球杆的瞄准方向；
点击事件：响应用户点击游戏界面上的各种按钮事件。

`overGame` 函数，游戏结束时调用该函数，同时发送游戏结束消息通知应用的其他组建。


## D. 项目演示
![游戏进行中](https://gitlab.com/szsl-doc/detail/-/raw/main/images/billiards_prince/billiards_prince_2.gif)

###   [演示视频 ⏯](https://www.bilibili.com/video/BV19j421971m/)

### 安装试用
[下载⏬安装包，试用app](https://gitlab.com/szsl-doc/detail/-/blob/main/apks/billiards_prince.apk)


## E. 项目源码
![源码](https://gitlab.com/szsl-doc/detail/-/raw/main/images/billiards_prince/billiards_prince1codes.png)

#### 关注公众号『数字森林』，后台发送：`台球`，获取源码。

![数字森林](https://gitlab.com/szsl-doc/detail/-/raw/main/images/gzh-szsl_ref-105.png)
