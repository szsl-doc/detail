# 切鱼达人，Android休闲游戏开发
> 使用 Android Studio 开发了一款休闲游戏 —— 《切鱼达人》

#### 关键字：<mark>切鱼</mark>

## A. 项目描述
《切鱼达人》是一款有趣的休闲游戏app，让玩家在虚拟的海洋世界中体验切割各种鱼类的乐趣。

《切鱼达人》玩法简单而富有挑战性。玩家需要在游戏中扮演一名渔民，使用手指在屏幕上滑动，将不断出现的各种鱼类切割成块或片。
游戏中的鱼类种类多样，玩家需要准确地切割它们，并躲避障碍物。挑战玩家的反应速度和手眼协调能力。

![游戏动效](https://gitlab.com/szsl-doc/detail/-/raw/main/images/fishcut/fish4.gif)

## B. 开发工具
- Android Studio Dolphin | 2021.3.1 Patch 1
- Java , JDK 11.0.13
- Gradle , gradle-7.4

## C. 代码设计
### Splash界面
![Splash界面](https://gitlab.com/szsl-doc/detail/-/raw/main/images/fishcut/fish-1.png)

Splash界面 (`SplashActivity`) 是游戏的首页，它展示了游戏的得分信息。
用户可以通过点击“游戏开始”按钮进入游戏页面，开始游戏的旅程。
同时，用户还可以点击“设置”按钮对app进行配置，个性化的游戏体验。

### 游戏主界面
![游戏主界面](https://gitlab.com/szsl-doc/detail/-/raw/main/images/fishcut/fish-2.png)


#### 鱼类“飞行”元素
`FlyingElement` 是《切鱼达人》游戏中的一个功能模块，用于定义和实现界面中不断出现的各种鱼类元素。
定义鱼类属性：FlyingElement 可以定义鱼类元素的属性，包括位置、旋转角度、移动速度等。这些属性将决定鱼类在游戏界面上的外观和行为。
碰撞检测：FlyingElement 还负责处理鱼类元素之间的碰撞检测。它会监测玩家切割刀子与鱼类元素的碰撞情况，以确定玩家是否成功切割鱼类，并计算得分。
控制鱼类切割元素的生成：FlyingElement 负责在游戏界面上控制鱼类切割元素的生成，并控制它们的旋转方向。

#### 自定义游戏示图
`GameView`是自定义游戏示图，它继承自`SurfaceView`，它的顶部栏展示了游戏得分和生命值；它的中部是游戏交互界面，各种鱼类`FlyingElement`在其中出现，用户用手指在其中滑动切割鱼块；它的底部栏放置了“暂停”按钮，用于控制游戏中途停止。

`GameView`是一个自定义的游戏视图，它继承自 `SurfaceView`。使用 `SurfaceView` 的好处是它允许在独立线程中进行画面的绘制，这样可以避免主线程因为耗时的绘制操作而导致的界面卡顿，保持游戏的流畅性。
在`GameView`示图上，顶部栏展示了游戏的得分和生命值，让玩家可以随时了解自己的游戏进展。中部是游戏的交互界面，在这里各种鱼类 `FlyingElement` 会不断地出现，用户可以通过手指在界面上滑动来切割鱼块。底部栏放置了一个“暂停”按钮，方便玩家在游戏进行中临时停止，以便处理其他事务。
通过这些设计，`GameView` 为玩家提供了一个互动丰富、操作便捷的游戏界面。

## D. 项目演示

[演示视频 ⏯](https://www.bilibili.com/video/BV1pZ4y1n7Pi/)

### 安装试用
[下载⏬安装包，试用app](https://gitlab.com/szsl-doc/detail/-/blob/main/apks/FishCuttingMaster.apk)

## E. 项目源码

![项目代码截图](https://gitlab.com/szsl-doc/detail/-/raw/main/images/fishcut/fish-code.png)


关注公众号『数字森林』，后台发送关键字：**`切鱼`**，获取项目源码。

![数字森林](https://gitlab.com/szsl-doc/detail/-/raw/main/images/gzh-szsl_ref-105.png)
