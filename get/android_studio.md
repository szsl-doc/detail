# 开发工具下载
## Android Studio Koala | 2024.1.1
Installers
- ChromeOS: [android-studio-2024.1.1.11-cros.deb](https://redirector.gvt1.com/edgedl/android/studio/install/2024.1.1.11/android-studio-2024.1.1.11-cros.deb) (992.0 MB)
- Mac (Apple Silicon): [android-studio-2024.1.1.11-mac_arm.dmg](https://redirector.gvt1.com/edgedl/android/studio/install/2024.1.1.11/android-studio-2024.1.1.11-mac_arm.dmg) (1.2 GB)
- Mac (Intel): [android-studio-2024.1.1.11-mac.dmg](https://redirector.gvt1.com/edgedl/android/studio/install/2024.1.1.11/android-studio-2024.1.1.11-mac.dmg) (1.3 GB)
- Windows (64-bit): [android-studio-2024.1.1.11-windows.exe](https://redirector.gvt1.com/edgedl/android/studio/install/2024.1.1.11/android-studio-2024.1.1.11-windows.exe) (1.2 GB)

```
SHA-256 checksums
96133c24b890ffa6dbee8976ec996391b68ac2e5288516806bc5e5c74382162e android-studio-2024.1.1.11-cros.deb
1d12468a766fbade3d02ec2cd0da7805253bf3dace8025747688f593d44465f5 android-studio-2024.1.1.11-mac_arm.dmg
70043577e0c2d9e688796951cf4359fb8755e436b4819d964633d2739846534d android-studio-2024.1.1.11-mac.dmg
eaf9fcec291e4be5b0b2f2fdcef15bfcc60df7303243c9ec0b00cd2cb7b37a72 android-studio-2024.1.1.11-windows.exe
```


## Android Studio Dolphin | 2021.3.1 Patch 1 
Installers
- ChromeOS: [android-studio-2021.3.1.17-cros.deb](https://redirector.gvt1.com/edgedl/android/studio/install/2021.3.1.17/android-studio-2021.3.1.17-cros.deb) (778.2 MB)
- Mac (Apple Silicon): [android-studio-2021.3.1.17-mac_arm.dmg](https://redirector.gvt1.com/edgedl/android/studio/install/2021.3.1.17/android-studio-2021.3.1.17-mac_arm.dmg) (1.0 GB)
- Mac (Intel): [android-studio-2021.3.1.17-mac.dmg](https://redirector.gvt1.com/edgedl/android/studio/install/2021.3.1.17/android-studio-2021.3.1.17-mac.dmg) (1.0 GB)
- Windows (64-bit): [android-studio-2021.3.1.17-windows.exe](https://redirector.gvt1.com/edgedl/android/studio/install/2021.3.1.17/android-studio-2021.3.1.17-windows.exe) (957.3 MB)

```
SHA-256 checksums
4d0c442d806fa8651c8e1baade6586c70aa46a61790aac0e91dfb4d5be7a7213 android-studio-2021.3.1.17-cros.deb
0adbbddfa1e0e52e7bf21a5b560f60f8982ef82c0677db2d2ff7a2bd73ab156f android-studio-2021.3.1.17-mac_arm.dmg
4e10799559efc3445d61fb12bbf68e0a9801607a6114c6783bb26a93784d3150 android-studio-2021.3.1.17-mac.dmg
dd176791e15e921d4a3b3c9a251c61e5cfd28d75588fd717971dfbac030cd497 android-studio-2021.3.1.17-windows.exe
```

## Android Studio 历史版本下载地址⏬
https://www.androiddevtools.cn/android-studio.html
