# 打砖块，Android休闲小游戏开发
使用 Android Studio 开发一款休闲游戏《打砖块》

#### 关键词：<mark>打砖块</mark>

## A. 项目描述
《打砖块》是一款经典的休闲小游戏 ，结合了经典的图形和音效，给玩家带来了轻松愉快的游戏体验。
该游戏操作简单易上手。玩家只需通过触摸屏幕控制底部的“拍子”左右移动，以反弹“小球” 击碎 顶部的砖块。玩家可以根据球的角度和速度调整“拍子”的位置，以便更好地击碎砖块并获得高分。
该游戏注重用户体验和界面设计。游戏界面简洁清晰，色彩搭配和谐，给人一种舒适愉悦的感觉。游戏操作流畅自然，音效效果逼真，让玩家沉浸其中。

![动效](https://gitlab.com/szsl-doc/detail/-/raw/main/images/hitbricks/g-hitbricks.gif)

## B. 开发工具
- Android Studio Dolphin | 2021.3.1 Patch 1
- Java , JDK 11.0.13
- Gradle , gradle-7.4

## C. 代码设计

![游戏主界面](https://gitlab.com/szsl-doc/detail/-/raw/main/images/hitbricks/hitbricks%20-s01.png)

### 界面设计
游戏主界面的上部，展示当前的得分、历史最高分以及生命值，这部分的信息由 `TextView` 显示。
以下的部分为玩家的操控界面，这部份的采用了自定义示图来实现，即 `GameView` 类，它继承自 `GLSurfaceView`，负责游戏画面的绘制和响应用户的操作。
> `GLSurfaceView` 是可以使用`OpenGL`的 `SurfaceView`，它继承自 `SurfaceView`，具备 `SurfaceView` 的特性，并加入了EGL的管理，它自带了一个`GLThread`绘制线程，绘制的工作直接通过`OpenGL`在绘制线程进行，不会阻塞主线程。

-  `GameView` 的绘制函数 —— `onDrawFrame()`，逐帧绘制画面。
```java
        /* onDrawFrame():每帧都通过该方法进行绘制。
         */
        public void onDrawFrame(GL10 gl) {
							......
								
            // 一个简单的游戏循环实现
            int frame_counter = 0;
            while (mLag >= Config.MS_PER_UPDATE) {

                // 如果游戏结束或暂停，则停止更新状态并冻结最后一帧，以便用户可以看到发生了什么。
                if (!State.getGamePaused() && !State.getGameOver()) {
                    mGame.updateState();
                }
                mLag -= Config.MS_PER_UPDATE;

                // 如果设备速度太慢，无法保持良好的帧速率，则跳过游戏处理，以便游戏在该设备上运行速度较慢。
                if (frame_counter >= Config.FRAME_SKIP) {
                    break;
                }
                frame_counter++;
            }

            gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
            mGame.drawElements(gl);
        }
```

-  `GameView` 监听屏幕触摸事件 —— `onTouchEvent()`，响应用户的操作。
```java
    public boolean onTouchEvent(MotionEvent e) {
        switch (e.getAction()) {
            case MotionEvent.ACTION_MOVE:

									......

                // 更新球拍位置
                mRenderer.updatePaddlePosition(resultWorldPos[0]);
                break;

            case MotionEvent.ACTION_DOWN:
                // 仅当用户单击屏幕时才开始游戏
                State.setGamePaused(false);
                break;
        }
        return true;
    }
```

### 元素设计
游戏的元素包含如下：
- 球拍，位于界面的底部，作用是接住小球、控制小球的反弹方向，用橙色来表示；
- 小球，在界面中上下运动，击碎砖块，用橙色来表示；

- 砖块：
	- 普通砖块，击打一次就破碎，用白色表示；
	- 硬砖块，需要击打两次才能击碎，用深蓝色表示；
	- 运动砖块，该砖块是左右移动的，位置变化，要击中它有一定挑战，用黄颜色表示；
	- 爆炸砖块，击中之后回发生爆炸，同时会将相邻的砖块炸碎，用红色表示；
	- 碎砖块，用于展现爆炸之后的碎屑效果。

这些元素具有一些的共同的属性和行为，因此设计了共同的基类 `Rectangle` ，规范定义了通用的属性和函数。

## D. 项目演示

[演示视频 ⏯](https://www.bilibili.com/video/BV1xc411k7bS/)

### 安装试用
[下载⏬安装包，试用app](https://gitlab.com/szsl-doc/detail/-/blob/main/apks/hitbricks-demo.apk)

## E. 项目源码

![项目代码截图](https://gitlab.com/szsl-doc/detail/-/raw/main/images/hitbricks/hitbricks%20-codes.png)

关注公众号『数字森林』，后台发送关键字：**打砖块**，获取项目源码。

![数字森林](https://gitlab.com/szsl-doc/detail/-/raw/main/images/gzh-szsl_ref-105.png)
