# 寻路迷宫，Android休闲益智小游戏开发
> 使用 Android Studio 开发了一款休闲益智小游戏——《寻路迷宫》。

#### 关键词：<mark>迷宫</mark>

## A. 项目描述

《寻路迷宫》是一款非常有趣的小游戏app，玩家需要寻找到迷宫的出口，挑战自己的智力和反应能力。

在游戏中，玩家需要操作角色前进、转向等动作，避免被障碍物（迷宫墙）阻挡，玩家需要思考解决方案，如何才能在最短的时间内到达出口。

《寻路迷宫》是一款非常有趣和挑战性的小游戏app，无论是休闲娱乐还是挑战自我都可以在游戏中找到快乐。

![动效](https://gitlab.com/szsl-doc/detail/-/raw/main/images/maze/minos.gif)

## B. 开发工具
- Android Studio Dolphin | 2021.3.1 Patch 1
- Java , JDK 11.0.13
- Gradle , gradle-7.4

## C. 代码设计

![游戏主界面](https://gitlab.com/szsl-doc/detail/-/raw/main/images/maze/mino-b.png)

该游戏界面的主体——迷宫区域，由自定义示图实现，即 `GameView`.
`GameView` 继承自 `View`，其实现了迷宫游戏的界面绘制和游戏逻辑的实现。
- `GameView` 的 `onDraw()` 函数 绘制了游戏的画面，
```java
public void onDraw(Canvas canvas) {
	    super.onDraw(canvas);
	    gameRenderer.render(canvas);
	    
	    if (inBackground) {
	        gameRenderer.renderOverlay(canvas);
	    }
	    
	}
```
绘制游戏画面的具体工作交给了 `GameRenderer` 来实现，
```java
public void render(Canvas canvas) {
        CLogger.d(TAG, "render");

        // 渲染迷宫墙壁，砌墙
        renderWalls(canvas);
        // 渲染迷宫目的地
        renderDestination(canvas);
        // 渲染 受用户拖动的圆形球
        renderPlayer(canvas);
    }
```

- `GameView` 的 `onTouchEvent()` 函数处理了用户的操作事件，
```java
    public boolean onTouchEvent(MotionEvent ev) {
        if (gameConroller != null) {
            gameConroller.handleDrag(this, ev);
        }
        return true;
    }
```
处理用户操作事件的具体工作交给了 `GameController`来做，这样做的好处是有利于代码解耦；
```java
    /* --- 操控"小球"拖放 -------------------------------------- */
    public boolean handleDrag(View view, MotionEvent ev) {

        if (game.getState() == GameState.PLAYING) {
            final int action = ev.getAction();

            switch (action) {
            case MotionEvent.ACTION_DOWN: {
                // 处理手指按下事件
                break;
            }
            case MotionEvent.ACTION_MOVE: {
								 // 处理手指移动事件
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                isDragging = false;
                break;
            }

            case MotionEvent.ACTION_POINTER_UP: {
                // 用户的一个手指离开了触摸屏 (但是还有其他手指还在触摸屏上)
                break;
            }
            }
        }
        return true;
    }
```

## D. 项目演示

![游戏演示](https://gitlab.com/szsl-doc/detail/-/raw/main/images/maze/display-minos.png)

### [演示视频 ⏯](https://www.bilibili.com/video/BV1CQ4y1L7S9/)

### 安装试用
[下载⏬安装包，试用app](https://gitlab.com/szsl-doc/detail/-/blob/main/apks/minos-app-demo.apk)

## E. 项目源码

![项目代码截图](https://gitlab.com/szsl-doc/detail/-/raw/main/images/maze/minos-codes.png)

关注公众号『数字森林』，后台发送关键字：**迷宫**，获取项目源码。

![数字森林](https://gitlab.com/szsl-doc/detail/-/raw/main/images/gzh-szsl_ref-105.png)
