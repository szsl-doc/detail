# “牛弹琴”，Android 弹钢琴 app 开发

使用 Android Studio 开发一款弹钢琴app
#### 关键词：<mark>弹钢琴</mark>

## A. 项目描述
本项目主要实现了【钢琴键盘的模拟】、【弹奏引导】以及【乐曲库】等功能。
钢琴键盘模拟：提供全尺寸键盘，并且根据用户的喜好来调整键盘的颜色样式。
弹奏引导：用户可以根据键盘上的提示符号 👆 来学习演奏。对于钢琴萌新/小白来说这个功能太方便了，可以像老手一样轻松弹奏各种曲目。
乐曲库：内置了曲谱，包括经典音乐、流行歌曲。用户可以跟随引导演奏这些曲目。
总之，这个app可以使得学习钢琴变得有趣和便捷，无论是钢琴初学者还是已经有一定经验的音乐爱好者，都可以从中受益。

![动效](https://gitlab.com/szsl-doc/detail/-/raw/main/images/piano/piano.gif)

## B. 开发工具
- Android Studio Dolphin | 2021.3.1 Patch 1
- Java , JDK 11.0.13
- Gradle , gradle-7.4

## C. 功能设计
### 钢琴键盘模拟

![键盘模拟](https://gitlab.com/szsl-doc/detail/-/raw/main/images/piano/piano-p1.png)

`PianoCanvasView`类实现了钢琴键盘的绘制，它继承`SurfaceView`，可以在子线程中更新UI；
- `draw_all_keys`方法，绘制了钢琴的所有键；
```java
    void draw_all_keys(final Canvas canvas) {
        // 重置 canvas
        {
            Paint p = new Paint();
            p.setColor(Color.BLACK);
            canvas.drawPaint(p);
        }

        if (KEY_COLORS == null || null == PRESSED_KEY_COLORS || KEY_COLORS.length <1 || PRESSED_KEY_COLORS.length <1) {
            setKeyboardStyleColorful();
        }

        for (int i = 0; i < piano.get_keys_count(); i += 2) {
            // 绘制大琴键
            final int col_idx = (i / 2) % KEY_COLORS.length;
            Paint big_key_paint = new Paint();
            big_key_paint.setColor(piano.is_key_pressed(i) ? PRESSED_KEY_COLORS[col_idx] : KEY_COLORS[col_idx]);
            Piano.Key key = piano.get_area_for_key(i);
            if (key != null) {
                draw_key(canvas, key, big_key_paint);

                piano.onDrawPianoKey(this, canvas, i, key);
            }
        }

        // 在大键之后绘制小键
        for (int i = 1; i < piano.get_keys_count(); i += 2) {
            // 绘制小琴键
            Paint flat_key_paint = new Paint();
            flat_key_paint.setColor(piano.is_key_pressed(i) ? Color.GRAY : 0xFF333333);
            Piano.Key key = piano.get_area_for_flat_key(i);
            if (key != null) {
                draw_key(canvas, key, flat_key_paint);
            }
        }

        appConfigHandler.onPianoRedrawFinish(this, canvas);
        piano.onPianoRedrawFinish(this, canvas);
    }
```

- `onTouchEvent` 方法捕捉屏幕上手指按下、抬起等动作；

### 弹奏引导
app通过在钢琴键上绘制手指符号（👆）来引导用户弹奏曲目，用户每按下一个键就发出一个对应的琴音。跟随着手指符号（👆）的变化，用户就可以弹奏出一个完整的曲调了。

1.  在`PianoConst`类中，`note2keyIdx`常量就保存了音符与琴键位置的映射关系；
2. 在`Piano`类中，`updateMelodyFromAssets`方法将乐曲资源转换成音符集合；
3. 通过 `Piano`类的`draw_emoji_on_piano_key`方法将引导手指符号（👆）绘制在对应的琴键上。
```java
    private void draw_emoji_on_piano_key(PianoCanvasView piano, Canvas canvas, int key_idx) {
        if (piano == null || canvas == null || key_idx < 0) {
            return;
        }
        // black: 1, 3, 7, 9, 11, 15 —— 奇数
        // white: 偶数

        // 👆 ✋
        String emoji = "\uD83D\uDC46";
        if (key_idx % 2 == 1) {
            piano.draw_emoji_on_black_key(canvas, emoji, key_idx);
        } else {
            piano.draw_emoji_on_white_key(canvas, emoji, key_idx);
        }
    }
```
### 乐曲库

![乐曲库](https://gitlab.com/szsl-doc/detail/-/raw/main/images/piano/piano-p2.png)

乐曲库是内置的，在`assets/jsongs`目录下，`json`格式文件保存；
乐曲选择界面是 `SongsActivity`，使用`RecyclerView` 展现乐曲列表，供用户选择；

## D. 项目演示

[演示视频 ⏯](https://www.bilibili.com/video/BV1oh4y1q7mZ/)

### 安装试用
[下载⏬安装包，试用app](https://gitlab.com/szsl-doc/detail/-/blob/main/apks/GF_piano.apk)

## E. 项目源码

![源码](https://gitlab.com/szsl-doc/detail/-/raw/main/images/piano/project.png)

关注公众号『数字森林』，后台发送：**弹钢琴**，获取源码。

![数字森林](https://gitlab.com/szsl-doc/detail/-/raw/main/images/gzh-szsl_ref-105.png)
