# 开心打地鼠，Android小游戏开发
#### 关键词：<mark>打地鼠</mark>

## A. 项目描述
“开心打地鼠”是一款非常有趣的游戏，它能够帮助人们放松身心，同时也能够锻炼人们的智力。

“开心打地鼠”这款游戏的玩法非常简单，玩家需要在规定的时间内点击屏幕上出现的地鼠，每次点击都可以得到一定的分数。但是，玩家需要小心，因为如果没有点击中，地鼠逃过的话，就会失去分数。游戏的难度会随着时间的推移而逐渐增加，需要玩家不断提高自己的反应能力和准确性。

![动效](https://gitlab.com/szsl-doc/detail/-/raw/main/images/tap-gopher/tap-gopher.gif?ref_type=heads)

## B. 开发工具
- Android Studio Dolphin | 2021.3.1 Patch 1
- Java , JDK 11.0.13
- Gradle , gradle-7.4

## C. 代码设计
`GameSurfaceView` 类定义了游戏的主要界面，它继承自 `SurfaceView`，采用这样的设计可以方便游戏画面在子线程中进行更新。

- `surfaceCreated`方法中，初始化图片、声音等资源，同时开启了**游戏线程**；
```java
public void surfaceCreated(SurfaceHolder holder) {
        GameSize.setView(this);
        SCREEN_W = this.getWidth();
        SCREEN_H = this.getHeight();
        // 初始化，图片、声音等
        initializeGameX();
        // 游戏线程
        Thread main_thread = new Thread(this);
        main_thread.start();
        game_th_on = true;

    }
```

- 在**游戏线程**当中，定时执行一系列的操作：游戏画面的绘制、执行游戏逻辑、游戏音乐的播放控制
 ```java
    @Override
    public void run() {
        while (game_th_on) {
            long starttime = System.currentTimeMillis();
            gameDraw();  // 游戏画面的绘制
            gameLogic(); // 游戏逻辑
            gamePlay();  // 游戏音乐的播放控制
            long endtime = System.currentTimeMillis();
            long delta = endtime - starttime;
            if (delta < thread_time) {
                try {
                    Thread.sleep(delta);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
```

## D. 项目演示

![游戏进行中](https://gitlab.com/szsl-doc/detail/-/raw/main/images/tap-gopher/s3.jpg)

### [演示视频 ⏯](https://www.bilibili.com/video/BV1WG411X7Cw/)

### 安装试用
[下载⏬安装包，试用app](https://gitlab.com/szsl-doc/detail/-/blob/main/apks/TapGopher.apk)

## E. 项目源码
![游戏源码](https://gitlab.com/szsl-doc/detail/-/raw/main/images/tap-gopher/codes-aaa.png)

关注公众号『数字森林』，后台发送：**打地鼠**，获取源码。

![数字森林](https://gitlab.com/szsl-doc/detail/-/raw/main/images/gzh-szsl_ref-105.png)
