# 单词精灵，Android 记单词 app 开发
> 使用 Android Studio 开发了一款 记单词 app —— 《单词精灵》

![动效](https://gitlab.com/szsl-doc/detail/-/raw/main/images/wordgenie/wordgenie.gif)

#### 关键词：<mark>单词精灵</mark>

## A. 项目描述
《单词精灵》是一款专为Android平台设计的单机记单词应用。该应用旨在帮助用户系统、高效地扩展词汇量，提升英语水平。应用内置丰富的词库和记忆方法，让用户在轻松愉悦的学习氛围中不断提升自己的词汇量。
这款应用提供了单词拼写、单词查询等功能，通过不同的方式让用户更深入地记忆单词。


## B. 开发工具
- Android Studio Dolphin | 2021.3.1 Patch 1
- Java , JDK 11.0.13
- Gradle , gradle-7.4

## C. 代码设计
### C1、数据库
项目先创建数据库 `wordset.db`文件，然后将 `res/raw` 中的词典文件导入数据库当中，接下来通过读文件的方式，从数据库中读取单词信息。
```java
    // 创建数据库
	mSQLiteDatabase=this.openOrCreateDatabase("wordset.db",MODE_PRIVATE,null);
	
    // 将 res/raw 中的词典文件 导入数据库当中
	private void insetdata(String Tablename) {
		if (Tablename == null) {
			return;
		}
		if(Tablename.equals("word4")){
			ins=getResources().openRawResource(R.raw.fourthlevel);
		}
		else if(Tablename.equals("word6")){
			ins=getResources().openRawResource(R.raw.sixthlevel);
		}
		else{
			ins=getResources().openRawResource(R.raw.college);
		}
		createTable(Tablename);
		m_ProgressBar.setVisibility(View.VISIBLE);
		m_ProgressBar.setProgress(0);
		m_ProgressBar.setMax(100);
		try{
			InputStreamReader inR=new InputStreamReader(ins);
			BufferedReader BR=new BufferedReader(inR);
			String str=BR.readLine();
			while(str!=null)
			{
				String[] temp=str.split(" ");
				AddData(temp[0],temp[1],Tablename);
				str=BR.readLine();

			}
			inR.close();
			ins.close();
			BR.close();
		}
		catch(FileNotFoundException e)
		{
			System.out.println(e);
		}
		catch(IOException e)
		{
			System.out.println(e);
		}
		new Thread(new Runnable(){

			public void run() {
				for(int i=0;i<10;i++){
					try{
						intCounter = (i+1)*20;
						Thread.sleep(1000);
						if(i==4){
							Message msg = new Message();
							msg.what = STOP;
							MyDataActivity.this.mHandler.sendMessage(msg);
							break;
						}else{
							Message msg= new Message();
							msg.what = NEXT;
							MyDataActivity.this.mHandler.sendMessage(msg);
						}
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}
			}

		}){

		}.start();

		mHandler = new Handler() {
			public void handleMessage(Message msg) {
				switch(msg.what){
					case STOP://到了最大值
						m_ProgressBar.setVisibility(View.GONE);
						Thread.currentThread().interrupt();//中断当前线程.
						break;
					case NEXT:
						if(!Thread.currentThread().isInterrupted()){//当前线程正在运行
							m_ProgressBar.setProgress(intCounter);
							MyDataActivity.this.setProgress(intCounter*100);
							MyDataActivity.this.setSecondaryProgress(intCounter*100);
						}
						break;
				}
				super.handleMessage(msg);
			}
		};
	}

	//创建表
	public void createTable(String tablename){
		try{
			mSQLiteDatabase.execSQL("CREATE TABLE "+tablename+"("+Table_ID
					+" INTEGER PRIMARY KEY autoincrement, "+English +" char[20] ,"+ Meaning+" TEXT );");
		}catch(Exception e){

		}
	}
	
    // 插入单条词汇以及对应的含义
	public void AddData(String english,String mean,String tablename){
		ContentValues cv=new ContentValues();
		cv.put(English,english);
		cv.put(Meaning, mean);
		mSQLiteDatabase.insert(tablename, null, cv);
	}

```


### C2、单词查询
点击查询按钮式，调取 `mSQLiteDatabase`的 `query`函数获得 `Cursor` 对象，然后调用`Cursor` 对象的 `getColumnIndex`函数获取单词对应字段的信息，最后通过`AlertDialog`控件展示出来。
```java
            // 查询单词含义
				Cursor S_cursor=SearchData(english,tablename);
				if(S_cursor.getCount()!=0){
					elish =S_cursor.getString(S_cursor.getColumnIndex("English"));
					mean=S_cursor.getString(S_cursor.getColumnIndex("Meaning"));
					openOptionsDialog();
				}

    // 查询数据库
	public Cursor SearchData(String english,String tablename) throws SQLException{
		Cursor mCursor =
				mSQLiteDatabase.query(true, tablename,new String[]{"_id", "English","Meaning" },
						"English" + "=?" ,new String[]{english},null,null,null,null);
		if(mCursor!=null){
			mCursor.moveToFirst();
		}
		return mCursor;
	}

    // 展示查询结果
	private void openOptionsDialog() {
		new AlertDialog.Builder(this)
				.setTitle("查询结果").setMessage(elish+"\n"+mean).show();
	}

```

## D. 项目演示
1. 主页展示了 `单词测试` 和 `导入词典`的功能。
![home](https://gitlab.com/szsl-doc/detail/-/raw/main/images/wordgenie/wg1.png)

2. 词典包含了 四级、六级和大学英语词汇。
![dicts](https://gitlab.com/szsl-doc/detail/-/raw/main/images/wordgenie/wg2.png)

3. 词汇列表页，包含词汇查询功能，并将单词以列表的形式展示出来。
![list](https://gitlab.com/szsl-doc/detail/-/raw/main/images/wordgenie/wg4.png)

4. 单词测试页面，看提示信息拼写单词，检验用户的单词的记忆情况 。
![list](https://gitlab.com/szsl-doc/detail/-/raw/main/images/wordgenie/wg5.png)

###   [演示视频 ⏯](https://www.bilibili.com/video/BV1fM4m197Vg/)

### 安装试用
[下载⏬安装包，试用app](https://gitlab.com/szsl-doc/detail/-/blob/main/apks/WordGenie.apk)


## E. 项目源码

#### 关注公众号『数字森林』，后台发送：`单词精灵`，获取源码。

![数字森林](https://gitlab.com/szsl-doc/detail/-/raw/main/images/gzh-szsl_ref-105.png)

